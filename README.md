<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>
-->

# Install Deps

```
gem install --file
```

or


```
bundle install
```

# Use

* create config.yaml with `access_token` (access token for example from Element -> Settings -> Help&About -> Reveal Token)
* have `room_directory.csv` in PWD
* `dangling_users.rb`
