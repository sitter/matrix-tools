#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.or

require 'faraday'
require 'json'
require 'yaml'

config_file = "#{__dir__}/config.yaml"
raise "Missing #{config_file}" unless File.exist?(config_file)

config = YAML.load_file(config_file)
connection = Faraday.new('https://kde.modular.im') do |c|
  c.request :json
  c.response :json
  c.use Faraday::Response::RaiseError
  # c.response :logger, ::Logger.new(STDOUT), body: true, bodies: { request: true, response: true }
  c.request :multipart
  c.request :url_encoded
  c.request :authorization, 'Bearer', config.fetch('access_token')
  c.adapter :excon
end

public_room_ids = []
room_ids_to_names = {}
offset = 0
loop do
  response = connection.get("/_matrix/client/v3/publicRooms?from=#{offset}&limit=500")
  body = response.body

  body.fetch('chunk').each do |room|
    next unless room.fetch('canonical_alias', nil)
    next unless room.fetch('room_id').end_with?(':kde.org')

    public_room_ids << room.fetch('room_id')
    room_ids_to_names[room.fetch('room_id')] = room.fetch('canonical_alias', '')
  end

  break unless body.include?('next_batch')

  offset = body['next_batch']
end
public_room_ids = public_room_ids.select { |id| id.start_with?('!') && id.include?(':') }

kicker = {}
public_room_ids.each_with_index do |id, index|
  response = connection.get("/_synapse/admin/v1/rooms/#{Faraday::Utils.escape(id)}/members")
  body = response.body

  members = body.fetch('members').select do |x|
    next false if x == '@appservice:libera.chat'

    x.end_with?(':libera.chat')
  end
  next if members.empty?

  kicker[id] = members
end

whoami_response = connection.get('/_matrix/client/v3/account/whoami')
my_user_id = whoami_response.body.fetch('user_id')

kicker.each do |room_id, members|
  warn "Going to kick from #{room_ids_to_names.fetch(room_id)} - #{members.size}"
end

kicker.each do |room_id, members|
  join = !members.include?(my_user_id)
  connection.post("/_matrix/client/v3/rooms/#{Faraday::Utils.escape(room_id)}/join") if join

  members.each do |member|
    connection.post("/_matrix/client/v3/rooms/#{Faraday::Utils.escape(room_id)}/kick",
                    reason: 'Cleaning up bridged libera.chat accounts', user_id: member)
  end
ensure
  connection.post("/_matrix/client/v3/rooms/#{Faraday::Utils.escape(room_id)}/leave") if join
end
