#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.or

require 'faraday'
require 'json'
require 'yaml'
require 'erb'

config_file = "#{__dir__}/config.yaml"
raise "Missing #{config_file}" unless File.exist?(config_file)

config = YAML.load_file(config_file)

matrix = Faraday.new('https://kde.modular.im') do |c|
  c.response :json
  c.request :json
  c.use Faraday::Response::RaiseError
  c.use Faraday::Response::Logger
  c.request :multipart
  c.request :url_encoded
  c.request :authorization, 'Bearer', config.fetch('access_token')
  c.adapter :excon
end

email_to_users = {}
csv = File.read('gitlab_users.csv').lines
csv.shift # header
parts = csv.collect { |line| line.strip.split(',') }
parts.each do |id, email, _name|
  raise if email_to_users.include?(email)

  email_to_users[email] = id
end

user_count = 0
external_count = 0
offset = 0
loop do
  response = matrix.get("/_synapse/admin/v2/users?from=#{offset}&limit=1000&guests=false")
  users_data = response.body

  users_data.fetch('users').each do |user|
    id = ERB::Util.url_encode(user.fetch('name'))
    next if id.start_with?('%40_telegram') # skip all telegram bridge accounts

    user_count += 1

    puts "requesting #{id}"
    response = matrix.get("/_synapse/admin/v2/users/#{id}")
    user_data = response.body

    unless user_data.fetch('external_ids').empty? # nothing to do here; already filled
      external_count += 1
      next
    end

    assigned = false
    user_data.fetch('threepids').each do |tpid|
      next unless tpid.fetch('medium') == 'email'

      validated_at = tpid.fetch('validated_at')
      raise if validated_at.zero? || validated_at == '0' || validated_at.nil?

      address = tpid.fetch('address')
      gitlab_id = email_to_users[address]
      next if gitlab_id.nil?
      raise if gitlab_id.empty?

      raise "already assigned #{address} #{id}" if assigned

      update = { 'external_ids' => [{ 'auth_provider' => 'kdeoidc', 'external_id' => gitlab_id }] }
      matrix.put("/_synapse/admin/v2/users/#{id}", update)
      assigned = true
      external_count += 1
    end
  end

  break unless users_data.include?('next_token')

  offset = users_data['next_token']
end

warn "external_count #{external_count}; user_count (without telegram) #{user_count}"
