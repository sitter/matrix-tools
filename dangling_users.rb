#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.or

require 'faraday'
require 'json'
require 'yaml'

config_file = "#{__dir__}/config.yaml"
raise "Missing #{config_file}" unless File.exist?(config_file)

config = YAML.load_file(config_file)
connection = Faraday.new('https://kde.modular.im') do |c|
  c.response :json
  c.use Faraday::Response::RaiseError
  c.use Faraday::Response::Logger
  c.request :multipart
  c.request :url_encoded
  c.request :authorization, 'Bearer', config.fetch('access_token')
  c.adapter :excon
end

csv = File.read('room_directory.csv').lines
csv.shift # header
public_room_ids = csv.collect { |line| line.split(',')[0] }
public_room_ids = public_room_ids.select { |id| id.start_with?('!') && id.include?(':') }

user_ids = []
user_ids_to_displaynames = {}
offset = 0
loop do
  response = connection.get("/_synapse/admin/v2/users?from=#{offset}&limit=1000&guests=false")
  body = response.body

  body.fetch('users').each do |user|
    user_ids << user.fetch('name')
    user_ids_to_displaynames[user['name']] = user.fetch('displayname', '')
  end

  break unless body.include?('next_token')

  offset = body['next_token']
end

user_ids_without_rooms = []
user_ids_without_public_rooms = []
user_rooms = {}

user_ids.each_with_index do |id, index|
  response = connection.get("/_synapse/admin/v1/users/#{Faraday::Utils.escape(id)}/joined_rooms")
  body = response.body
  user_rooms[id] = body['joined_rooms']
  if body['joined_rooms'].empty?
    user_ids_without_rooms << id
  elsif body['joined_rooms'].none? { |room| public_room_ids.include?(room) }
    user_ids_without_public_rooms << id
  end

  without = user_ids_without_rooms.size
  without_public = user_ids_without_public_rooms.size
  overall = without + without_public
  warn "--> Processed #{index + 1} entries. Without: #{without}, Without Public: #{without_public}; #{overall}"
end

user_ids_without_rooms = user_ids_without_rooms.map do |id|
  { 'id' => id, 'displayname' => user_ids_to_displaynames.fetch(id) }
end
File.write('user_ids_without_rooms.json', JSON.generate(user_ids_without_rooms))

user_ids_without_public_rooms = user_ids_without_public_rooms.map do |id|
  { 'id' => id, 'displayname' => user_ids_to_displaynames.fetch(id) }
end
File.write('user_ids_without_public_rooms.json', JSON.generate(user_ids_without_public_rooms))
